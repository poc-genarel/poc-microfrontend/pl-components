// Anything exported from this file is importable by other in-browser modules.
import Root from "./root.component";
export * as components from "./components";

export { Root };

export function publicApiFunction() {}
