export interface CustomButtonProps {
  label: string;
}

export const CustomButton = ({ label }: CustomButtonProps) => {
  return <button>{label}</button>;
};

export default CustomButton;
