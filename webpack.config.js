const { merge } = require("webpack-merge");
const glob = require("glob");
const singleSpaDefaults = require("webpack-config-single-spa-react-ts");

module.exports = (webpackConfigEnv, argv) => {
  const defaultConfig = singleSpaDefaults({
    orgName: "plc",
    projectName: "components",
    webpackConfigEnv,
    argv,
  });

  /*
  * entry: glob
      .sync("./src/**")
        .filter((name) => !/\.(test|d)\.(ts|tsx)/.test(name))
        .filter((name) => /\.(ts|tsx)/.test(name))
        .reduce((acc, item) => {
            const path = item.split("/");
            const lastItem = path.at(-1).replace(/\.(tsx|ts)/, "");
            path.shift();
            path.shift();
            path.pop();
            path.push(lastItem);
            const name = path.join("/");
            acc[name] = item;
            console.log({ path, name, item, lastItem });
            return acc;
        }, {}),
        output: {
        filename: "[name].js",
            clean: true,
    },
  * */

  return merge(defaultConfig, {
    // modify the webpack config however you'd like to by adding to this object
    externals: ["@mui/material", "react-icons", "react-router-dom"],
  });
};
